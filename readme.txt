About / Why:
	I decided to add versioning support to my skeletonized developer game-mode base so that I can indirectly
	notify developers via server / game chat when a new version of the game-mode ( Minor to major change,
	additional functionality such as hooks, meta-table objects, helper-functions, potential security issue,
	etc... ) is released to ensure everyone is running the most current version of the dev base.

	I didn't want to manage the version.txt and be required to update it for each change I made by opening and
	editing the file directly... so I made a tool to do it for me ( some IDEs manage build-numbers for projects
	and similar things ) so I based my idea on the fact that similar things exist nut nothing as "simple" or in
	the current "IDE" I use for Lua development.


Future Functionality:
	* INI Configuration for projects / paths, optional "build" var, independent of version where a value can
		be continuously updated for each edit / change made.

	* Potential Multi-Project Support

	* User Requests



Modifying a .bat file.. Opening the file:
	Right-Click versioning_system.bat and choose Edit, or Edit in Notepad++ ( if used ) or select
	Open With and choose your favorite text editing program.


Notes:
	:: and REM at the beginning of a line denotes comments, as "should" ;'s...

	"'s are not used for paths

	%variable% is the typical way to "read" the contents of a variable

	%0 provides information regarding the 0th argument which is the .bat file itself

	%1-9 provides information regarding 1st through 9th argument used when launching a .bat file,
	while 9 is the "maximum" number of defined registers / variables to use, SHIFT can be used which
	will reassign all argument variables with the value of one greater than itself... 9 becomes the
	previously inaccessible 10th value. Continuing to call SHIFT will allow for theoretically unlimited
	arguments to be accessible.


Setup:
	As of right now, setup isn't as "easy" as it should be for end-users, however my guess is more technically
	savvy individuals will be using this as a way to quickly manage their version file without needing to open
	and edit it each time. In the future there will be an "easy" INI file to set file-paths and other options.

	For right now, setting the path is done manually... Two variables have been added to simplify the language
	and how paths are set up.

	NOTE: No spaces should be before the =, after the =, or at the end of the line. A trailing \ should be used
	for the version path, version data path, version folder, and version data folder variables. %_path% automatically
	includes a trailing \...

	Default:
		SET _version_path=%_path%
		SET _version_data_path=%_path%
		SET _version_folder=acecooldev_base\
		SET _version_data_folder=.version_data\

	Example 1:
		SET _version_path=Z:\dedicated_servers\server_1\garrysmod\gamemodes\
		SET _version_data_path=Z:\dedicated_servers\server_1\garrysmod\gamemodes\
		SET _version_folder=acecooldev_base\
		SET _version_data_folder=.acecooldev_base_versiondata\

	The Above will set the following complete paths:
		Z:\dedicated_servers\server_1\garrysmod\gamemodes\acecooldev_base\
		Z:\dedicated_servers\server_1\garrysmod\gamemodes\.acecooldev_base_versiondata\

	Example 2:
		SET _version_path=Z:\dedicated_servers\server_1\garrysmod\gamemodes\acecooldev_base\
		SET _version_data_path=Z:\dedicated_servers\server_1\garrysmod\gamemodes\.acecooldev_base_versiondata\

	The Above will set the following complete paths and shows that the 2 other variables don't need to exist
	but will still properly create everything without needing to make any other changes ( If this is easiest for you, do it ):
		Z:\dedicated_servers\server_1\garrysmod\gamemodes\acecooldev_base\
		Z:\dedicated_servers\server_1\garrysmod\gamemodes\.acecooldev_base_versiondata\

	Another option would be to keep the Default variables, and put the .bat files inside of the gamemodes folder. the %_path%
	variable will automatically use the current folder the bat is called from, then apply the _version_folder and _version_data_folder
	to the path for the complete file paths. It may be easier to do this depending how many projects exist.

	After that is done, update the Lua file variable: VERSION_HTTP_PATH to the URL for the version.txt... Ensure the version.txt
	is in a folder that gets uploaded with changes ( such as in the base dir as I did with the dev base )