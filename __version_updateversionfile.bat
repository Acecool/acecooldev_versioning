::
:: Versioning System - UpdateFile - Josh 'Acecool' Moser
:: Builds the x.y.z version data out of the 3 version files.
::

:: Don't output all the commands
@ECHO OFF

SET _path=%~dp0
SET _bat=%~n0
SET _version_file=%1
SET _version_build_file=%2
SET _version_minor_file=%3
SET _version_major_file=%4

:: Debugging
REM ECHO 1: %1
REM ECHO 2: %2
REM ECHO 3: %3
REM ECHO 4: %4

SET _build_data=0
SET _minor_data=0
SET _major_data=0

IF NOT EXIST %2 GOTO CheckMinor
SET /p _build_data=<%2

:CheckMinor
	IF NOT EXIST %3 GOTO CheckMajor
	SET /p _minor_data=<%3

:CheckMajor
	IF NOT EXIST %4 GOTO UpdateVersion
	SET /p _major_data=<%4

:UpdateVersion
	SET _version=%_major_data%.%_minor_data%.%_build_data%
	START /WAIT "Updating Version File to: %_version%" /NORMAL /MIN /I /D %_path% __version_save_file.bat %_version_file% %_version% 0.0.0 TRIM ""

	:: Restart management program...
	REM START "Version Management" /NORMAL /I /D %_path% versioning_system.bat
	EXIT