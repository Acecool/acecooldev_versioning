::
:: Version System - Josh 'Acecool' Moser
::

:: Don't output all the commands
@ECHO OFF

::
:: Vars
::


::
:: %0 is the information surrounding the bat file that is running.
:: _path is automatically assigned to the full path to this .bat file, no need to change...
:: _bat is automatically assigned to the current .bat filename, no need to change...
::
SET _path=%~dp0
SET _bat=%~n0


:: _version_folder should be the _version_path plus _version_folder path where you want version.txt to be stored.
:: _version_data_folder should be the _version_data_path plus _version_data_folder path where you want the version data to be stored ( can easily just use current directory )
::
:: Examples...
::	SET _version_path=Z:\path\to\where_the_version_txt_should_be_saved\
::	SET _version_data_path=Z:\path\to\where_the_data_should_be_stored\
::
::	SET _version_path=Z:\Users\Acecool\Dropbox\Public\tutoring\_systems\versioning_system\
::	SET _version_data_path=%_path%acecooldev_base_version_data\
::
::	SET _path=
::	SET _version_path=Z:\dedicated_servers\server_1\garrysmod\gamemodes\acecooldev_base\
::	SET _version_data_path=%_path%acecooldev_base_version_data\
::
:: These are examples of what the paths could look like and still work:
::

::
:: These are examples of what the paths could look like and still work ( Directly to GM dir ):
::

:: The first 2 vars are reused below which put the 2 together. Separated for "simplicity"... in case
:: the .bat file path is to be used to avoid confusion of how the pieces fit together until INI implementation is done.
SET _version_path=%_path%
SET _version_data_path=%_path%
SET _version_folder=acecooldev_base\
SET _version_data_folder=.version_data\


::
:: Color Option... First Character Represents Background, While the Second Character Represents Text Color.
::
::----------------------------------------------------------------------------
:: Greyscale Colors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::----------------------------------------------------------------------------
::	0 = Black		8 = Gray	7 = White		F = Bright White
::----------------------------------------------------------------------------
:: RGB Colors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::----------------------------------------------------------------------------
::	4 = Red		( DARK RED )	C = Light Red	( RED )
::	2 = Green	( DARK GREEN )	A = Light Green	( GREEN )
::	1 = Blue	( DARK BLUE )	9 = Light Blue	( BLUE )
::----------------------------------------------------------------------------
:: CMY Colors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::----------------------------------------------------------------------------
::	3 = Aqua	( DARK TEAL )	B = Light Aqua 		( CYAN )
::	5 = Purple	( PURPLE )		D = Light Purple 	( MAGENTA )
::	6 = Yellow	( DARK YELLOW )	E = Light Yellow 	( YELLOW )
::----------------------------------------------------------------------------

:: Alter the Console "COLOR BF", no quotes; B = Background, F = Foreground ( ie TEXT )...
COLOR F0


::
:: NO NEED TO EDIT BELOW UNLESS MAKING IMPROVEMENTS
::


::
:: This would be the FULL path to the version data... _version_path is the version.txt file which
:: should be in the game-mode folder.. The _version_data_path shouldn't be in the gm folder ( clutter ).
:: This path with %~dp0 is the current dir ( of the .bat ) plus 2 folders... acecooldev_base/ for version.txt
:: and acecooldev_base_version_data/ for the 3 Version Info files.
::
SET _version_path=%_version_path%%_version_folder%
SET _version_data_path=%_version_data_path%%_version_data_folder%


:: Physical File-Names for the Version File, and the 3 Version Info Files ( which prevents the need for string processing )
SET _version_file=%_version_path%version.txt
SET _version_build_file=%_version_data_path%versioninfo_build.txt
SET _version_minor_file=%_version_data_path%versioninfo_minor.txt
SET _version_major_file=%_version_data_path%versioninfo_major.txt

:: "Language" Data... Would make more sense in the menu instead of debug stuff which doesn't show up...
SET _L_VERSION="Version Management System"
SET _L_BUILD="Updating Build Version"
SET _L_MINOR="Updating Minor Version Number ( Resets Build to 0 )"
SET _L_MAJOR="Updating Major Version Number ( Resets Minor and Build to 0 )"

:: Save command
SET SAVE=START /WAIT %_L_VERSION% /NORMAL /MIN /I /D %_path% __version_save_file.bat
SET SAVE_ARGS=0 0 FAST


:GetCurrentVersion
	IF NOT EXIST %_version_file% GOTO DefaultVersion
	SET /p CurrentVersion=<%_version_file%
	GOTO MENU


:DefaultVersion
	SET CurrentVersion=0.0.0

	:: Notification
	ECHO Creating default files. If paths are incorrectly formatted, the files will be creating in the current directory ( If that is planned, so be it ): %_path%...& echo.
	ECHO If the paths are correctly formatted, but do not exist, the folders will be created for you.& echo.& echo.

	ECHO Version File ( Created after TIMEOUT ): %_version_file%& echo.& echo.
	ECHO File: x.0.0 = %_version_major_file%& echo.& echo.
	ECHO File: 0.x.0 = %_version_minor_file%& echo.& echo.
	ECHO File: 0.0.x = %_version_build_file%

	:: Setup files...
	%SAVE% %_version_build_file% %SAVE_ARGS% ""
	%SAVE% %_version_minor_file% %SAVE_ARGS% ""
	%SAVE% %_version_major_file% %SAVE_ARGS% ""

	:: Give the user time to read...
	TIMEOUT /t 30

	GOTO UpdateVersionFile


:MENU
	CLS
	ECHO	Current Project Version: %CurrentVersion%& echo.
	ECHO 1. Increment Build Number: 0.0.x
	ECHO 2. Decrement Build Number: 0.0.x& echo.

	ECHO 3. Increment Minor Number: 0.x.0 ..... [ RESETS BUILD ?? TO 0 ]
	ECHO 4. Decrement Minor Number: 0.x.0& echo.

	ECHO 5. Increment Major Number: x.0.0 ..... [ RESETS BUILD ?? AND MINOR TO 0 ]
	ECHO 6. Decrement Major Number: x.0.0& echo.

	ECHO 7. Quit.
	ECHO 8. Quit and Relaunch.& echo.

	ECHO 9. Reload Menu& echo.

	:: Choice doesn't "matter". If 4312 is used, 1=4, 2=3, 3=2, 4=1... Last Option must be first it seems...
	CHOICE /C:1234567890 /N /D 0 /T 9999 /M "Please select an option:"

	ECHO %ERRORLEVEL%

	:: Must be in REVERSE order...
	IF ERRORLEVEL == 9 GOTO GetCurrentVersion
	IF ERRORLEVEL == 8 GOTO EIGHT
	IF ERRORLEVEL == 7 GOTO SEVEN
	IF ERRORLEVEL == 6 GOTO SIX
	IF ERRORLEVEL == 5 GOTO FIVE
	IF ERRORLEVEL == 4 GOTO FOUR
	IF ERRORLEVEL == 3 GOTO THREE
	IF ERRORLEVEL == 2 GOTO TWO
	IF ERRORLEVEL == 1 GOTO ONE


:ONE
	ECHO %_L_BUILD%
	%SAVE% %_version_build_file% %SAVE_ARGS% +1
	GOTO UpdateVersionFile

:TWO
	ECHO %_L_BUILD%
	%SAVE% %_version_build_file% %SAVE_ARGS% -1
	GOTO UpdateVersionFile

:THREE
	ECHO %_L_MINOR%
	%SAVE% %_version_build_file% %SAVE_ARGS% ""
	%SAVE% %_version_minor_file% %SAVE_ARGS% +1
	GOTO UpdateVersionFile

:FOUR
	ECHO %_L_MINOR%
	%SAVE% %_version_minor_file% %SAVE_ARGS% -1
	GOTO UpdateVersionFile

:FIVE
	ECHO %_L_MAJOR%
	%SAVE% %_version_build_file% %SAVE_ARGS% ""
	%SAVE% %_version_minor_file% %SAVE_ARGS% ""
	%SAVE% %_version_major_file% %SAVE_ARGS% +1
	GOTO UpdateVersionFile

:SIX
	ECHO %_L_MAJOR%
	%SAVE% %_version_major_file% %SAVE_ARGS% -1
	GOTO UpdateVersionFile

:SEVEN
	GOTO PE

:EIGHT
	START "Version Management" /NORMAL /I /D %_path% %_bat%
	GOTO PE

:UpdateVersionFile
	START /WAIT %_L_VERSION% /NORMAL /MIN /I /D %_path% __version_updateversionfile.bat %_version_file% %_version_build_file% %_version_minor_file% %_version_major_file%
	ECHO Updating Version Management Files and Version File& echo.
	REM EXIT
	REM TIMEOUT /t 2 /nobreak -- No need for timeout with /WAIT added to START....
	GOTO GetCurrentVersion

:PE
	REM PAUSE
	EXIT