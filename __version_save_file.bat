::
:: Versioning System - UpdateFile - Josh 'Acecool' Moser
:: Arguments: file, data, default-data, save-mode ( TRIM, FAST ), update-mode ( +1, -1 )
::

:: Don't output all the commands
@ECHO OFF

SET _path=%~dp0
SET _bat=%~n0
SET _file=%1
SET _file_path=%~dp1
SET _data=%2
SET _default_data=%3
SET _file_data=%_default_data%
SET _savemode=%4
SET _updatemode=%5

:CheckDir
	IF EXIST %_file_path% GOTO CheckFile
	MKDIR %_file_path%
	Goto CheckFile

:CheckFile
	IF NOT EXIST %_file% GOTO CreateFile
	ATTRIB -R -H %_file%
	SET /p _file_data=<%_file%
	Goto HandleData

:CreateFile
	SET _data=%_default_data%

:HandleData
	IF %_updatemode% == "" GOTO HandleSaveFile
	IF %_updatemode% == +1 GOTO IncrementData
	IF %_updatemode% == -1 GOTO DecrementData
	GOTO HandleSaveFile

:IncrementData
	SET /a _file_data+=1
	GOTO UpdateFileData

:DecrementData
	SET /a _file_data-=1
	IF %_file_data% == -1 GOTO ResetToZero
	GOTO UpdateFileData

:ResetToZero
	SET _data=0
	GOTO HandleSaveFile

:UpdateFileData
	SET _data=%_file_data%	
	GOTO HandleSaveFile

:HandleSaveFile
	IF %_savemode% == "" GOTO SaveFileFast
	IF %_savemode% == FAST GOTO SaveFileFast
	IF %_savemode% == TRIM GOTO SaveFileSlow
	GOTO SaveFileFast

:SaveFileFast
	REM ECHO Writing File FAST
	>%1 ECHO %_data%
	GOTO End

:SaveFileSlow
	REM ECHO Writing File SLOW
	ECHO.|set /P = "%_data%" > %1
	Goto End

:End
	REM ECHO Inserted Data: "%_data%" Into File: "%_file%"
	REM PAUSE
	EXIT